from fastapi import FastAPI, Depends
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import FileResponse, Response
from fastapi.testclient import TestClient

import aioredis
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fastapi_cache.decorator import cache
from typing import Optional
import requests



app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")  # authentication with token

@cache
async def redis_cache():
    return 1

@app.get("/get-image")
@cache(expire=360)                                                                        # caching the function content variables for 360 seconds
async def get_image(word: Optional[str] = None, token: str = Depends(oauth2_scheme)):
    image_url = f"http://127.0.0.1:8080/monster/{word}"                                   # runned docker image of avatar generator
    response = requests.get(image_url).content                                            # image is got from response with python requests
    return Response(content=response, media_type="image/png")                             # returning image in response

@app.on_event('startup')
async def on_startup() -> None:
    redis = aioredis.from_url("redis://localhost", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")


client = TestClient(app)


def test_get_image():
    response = client.get("/get-image")
    assert response.status_code == 200

